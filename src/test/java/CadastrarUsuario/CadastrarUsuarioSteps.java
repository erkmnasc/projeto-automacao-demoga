package CadastrarUsuario;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import RealizarLogin.LoginLogics;
import io.cucumber.java.en.*;

public class CadastrarUsuarioSteps {

	WebDriver driver = null;

	LoginLogics login;
	CadastrarUsuarioLogics cadastrar;

	@Given("usuario esta na pagina inicial")
	public void usuario_esta_na_pagina_inicial() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to("https://demoqa.com/login");

	}

}