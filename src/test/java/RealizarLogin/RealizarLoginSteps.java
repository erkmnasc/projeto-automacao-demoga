package RealizarLogin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;

public class RealizarLoginSteps {
	WebDriver driver = null;

	LoginLogics login;

	@Given("usuario esta na pagina de login")
	public void usuario_esta_na_pagina_de_login() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to("https://demoqa.com/login");
	}

}