package AdicionarProdutos;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import RealizarLogin.LoginLogics;
import RemoverProdutos.RemoverProdutosLogics;
import io.cucumber.java.en.*;

public class AdicionarProdutoSteps {

	WebDriver driver = null;

	LoginLogics Login;
	AdicionarProdutoLogics AdicionarProdutosLogic;
	RemoverProdutosLogics RemoverProdutosLogic;

	@Given("usuario acessa a pagina inicial")
	public void usuario_acessa_a_pagina_inicial() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.navigate().to("https://demoqa.com/books");
	}

}