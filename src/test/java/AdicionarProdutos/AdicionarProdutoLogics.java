package AdicionarProdutos;

import java.time.Duration;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdicionarProdutoLogics {

	WebDriver driver;
	WebDriverWait wait;

	public AdicionarProdutoLogics(WebDriver driver) {
		this.driver = driver;
	}

	public void usuario_visualiza_colecao() {
		new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.urlToBe("https://demoqa.com/books"));
		String URL = driver.getCurrentUrl();
		Assert.assertEquals(URL, "https://demoqa.com/books");
	}

}