@ProjetoAutomacao
Feature: Remover Produtos Do Carrinho

  @RemoverProdutosDoCarinho
  Scenario: Remover Produtos Do Carrinho
    Given usuario loga no sistema
    And usuario adiciona um produto em seu carrinho
    When produto adicionado em seu carrinho
    And usuario acessa pagina de perfil
    And clica para remover os produtos
    Then produto removido