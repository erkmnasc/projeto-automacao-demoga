@ProjetoAutomacao
Feature: Registrar Usuario

  @RegistrarUsuario
  Scenario: Registrar Usuario
    Given usuario esta na pagina inicial
    When usuario clica para se cadastrar
    And usuario insere seu primeiro nome
    And usuario insere seu ultimo nome
    And usuario insere seu nome de usuario
    And usuario insere sua senha
    And usuario valida o captcha e clica em registrar
    Then usuario cadastrado com sucesso