@ProjetoAutomacao
Feature: Adicionar Produto No Carrinho

  @AdicionarProdutoNoCarrinho
  Scenario: Adicionar Produto No Carrinho
    Given usuario acessa a pagina inicial
    And usuario clica em login
    And usuario loga com sucesso
    When usuario visualiza os produtos disponiveis
    And usuario clica em um dos produtos
    And adiciona o produto ao seu carrinho
    Then produto e adicionado ao carrinho